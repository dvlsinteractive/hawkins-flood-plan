<?php include('includes/header.php'); ?>
	<div id="content" class="meetings group">
	
	<h1>Community Meetings</h1>
	<div class="intro">
		<span class="communityicon"></span>
		<p>Metro Water Services will hold the following community workshops in different regions across Davidson County. The purpose of these workshops is to review the proposed County-wide Flood Protection Plan, and most importantly, provide an opportunity for community members to ask questions and offer input on the proposed plan. Interactive working sessions will be held throughout the meetings, giving community members a chance to voice opinions and raise questions regarding different topics. Each of these meetings are open to all community members.</p>
	</div>

	<div class="schedule">

		<div class="event">
			<h2>Lakeshore Christian Church</h2>
			<p>5434 Bell Forge Ln E <br>
			<span class="date">January 20, 2018</span><br> 10:00 – 12:00pm</p>
		</div>

		<div class="event">
			<h2>Bellevue YMCA</h2>
			<p>8101 TN-100 <br>
			<span class="date">Thursday, February 22, 2018</span><br> 5:30-7:00pm</p>
		</div> 

		<div class="event">
			<h2>Hartman Park Community Center</h2>
			<p>2801 Tucker Rd <br>
			<span class="date">Saturday, March 10, 2018</span><br> 10:00am – 11:30am</p>
		</div>		 

		<div class="event">
			<h2>McGavock High School</h2>
			<p>3150 McGavock Pike <br>
			<span class="date">Wednesday, March 14, 2018</span><br> 5:30-7:00pm</p>
		</div>		 

		<div class="event">
			<h2>Downtown Library</h2>
			<p>615 Church St <br>
			<span class="date">Thursday, March 15, 2018</span><br> 11:00am – 12:30pm</p>
		</div>


	</div><!-- schedule -->


	</div>
<?php include('includes/footer.php'); ?>