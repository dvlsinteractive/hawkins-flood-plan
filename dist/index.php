<?php include('includes/header.php'); ?>
	<div id="content" class="home group">

		<h1>Advancing Davidson County’s Flood Protection System</h1>
		<div class="splitCol">
			<p>Nashville is poised to complete the next phase of a multifaceted, County-wide Flood Protection System to shield Metro from flooding disasters. Since the May 2010 flood, Metro Water Services has worked with local, state and federal agencies to improve public safety and minimize the potential impact of future floods. The investment to date—totaling more than $140 million since 2002—has increased public safety and reduced risk of property damage from floods in every Metro district.</p>
			<p>Davidson County is increasingly vulnerable to extreme weather conditions. It is now imperative that Metro government continue to support the County-wide Flood Protection System to safeguard the community.</p>
		</div>

		<hr class="droplet" />


		<div class="splitCol">

			<div>
				<img src="../webimages/flood.jpg" alt="downtown buildings during flood"/>
			</div>

			<div>
				<h2>Metro’s proactive approach</h2>
				<p>Since 2002, a total of $143.4 million in federal, state, and local funds have been invested in the County-wide Flood Protection System: $46.8 million has been allocated to home acquisition—removing homes that are susceptible to flooding; $3.7 millionhas been dedicated to NERVE and SAFE technologies to alert the public, first responders and Metro officials about extreme weather events; and $92.9 million has been used for additional mitigation programs, such as feasibility studies and preparedness plans, pump station improvements across the county and newly appointed “watershed advisors” who monitor tributaries throughout the county. As a result, Nashville has become a model in flood mitigation from which other cities are learning.</p>
			</div>

		</div>

		<img src="webimages/chart.png" class="chart" alt="chart of costs"/>

		<hr/>

		<h2>Metro Nashville Flood Protection Analysis</h2>

		<div class="threeCol">

			<div>
				<span class="lightgallery"><a href="webimages/Map-LID-1200.jpg" data-src="webimages/Map-LID-1200.jpg" class=""><img src="webimages/Map-LID-400.jpg" alt="non-profit project map" /><span class="visuallyhidden">View larger map</span></a></span>
				<h2>Non-profit &amp; Community Group Projects</h2>
				<p>This map is a view of the non-profit and community group recovery efforts since 2002. Several groups have made long-term commitments to rebuilding our community through beautification projects that include rain gardens and tree plantings. </p>
			</div>

			<div>
				<span class="lightgallery"><a href="webimages/Map-Non-Profit-1200.jpg" data-src="webimages/Map-Non-Profit-1200.jpg" class=""><img src="webimages/Map-Non-Profit-400.jpg" alt="stormwater project map" /></a></span>
				<h2>Stormwater Capital and Maintenance Projects</h2>
				<p>This map shows the County-wide view of one of the mostcritical components of the flood system improvements. When extreme weather conditions arise, this protects the countyfrom environmental and structural harm. It is essential forflood prevention measures.</p>
			</div>

			<div>
				<span class="lightgallery"><a href="webimages/Map-StormWater-1200.jpg" data-src="webimages/Map-StormWater-1200.jpg" class=""><img src="webimages/Map-StormWater-400.jpg" alt="LID project map" /></a></span>
				<h2>LID Projects</h2>
				<p>This map highlights how excessive rainfall is confined to specific areas that strengthens the environment and blocks large volumes of water from flowing to the stormwater system. Permitted projects include infill redevelopment (projects requiring green infrastructure as a result of regulated residential infill) and LID sites(projects implementing green infrastructure as a result of a full grading permit under Volume 4 or 5). </p>
			</div>

		</div>


	</div>
<?php include('includes/footer.php'); ?>
