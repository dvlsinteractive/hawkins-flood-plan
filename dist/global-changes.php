<?php include('includes/header.php'); ?>
<div id="content" class="globalchanges group">
	
	<h1>Global changes in weather impact Davidson County</h1>
	<div class="intro">
		
		<p><strong>Worldwide climate trends hit close to home</strong> <br/>Global weather patterns are changing, and the entire country is experiencing more extreme weather events than ever before. According to the National Oceanic and Atmospheric Administration (NOAA), extreme rain events have become more intense and more frequent since the early 1980s. In the Southeast, these events may occur up to two or three times as often by the end of the century. The Federal Emergency Management Agency (FEMA) reports that the U.S. has experienced more than 65 major flooding events in the past 20 years.</p>
	</div>

	<div class="splitCol">

			<div>
				<img src="../webimages/FEMA-Flood-related-Graph.jpg" alt="FEMA Flood Related Graph"/>
			</div>

			<div>
				<h2>FLOOD RELATED DISASTERS 1953-2015 (FEMA)</h2>
				<p>Flood-related disaster donations by the Federal Emergency Management Agency (FEMA) have been on the rise. In 2005, Hurricane Katrina struck the Gulf Coast—Louisiana, Mississippi and Alabama and caused more than $100 billion in damages. Five years later, Nashville experienced the flood of 2010, resulting in $2 billion in damages.</p>
				<ul>
					<li class="blue">Number of floods declared a disaster, using category “Flood.”</li>
					<li class="red">Number of floods declared a disaster, using “Flood” category and “Storm” category entries which mentioned “Flood” or “Flooding.”</li>
				</ul>
				
				<p><em>Data Source: Raw data accessed and downloaded from <a href="http://FEMA.gov/" target="_blank">FEMA.gov</a> data visualization on 8/12/16. Data published by OpenFEMA. </em></p>
			</div>

	</div>
	
	<hr/>
	
	<div class="splitCol">

			<div>
				<img src="../webimages/NOAA-precipitation-map.jpg" alt="Change in Perecipitation in the US Map"/>
			</div>

			<div>
				<h2>CHANGE IN PRECIPITATION IN THE UNITED STATES 1901-2015</h2>
				<p>According to the National Oceanic and Atmospheric Administration (NOAA), extreme rain events have become more intense and more frequent since the early 1980s. Almost the entire Cumberland River Basin has seen an increase in precipitation since 1900.</p>
				<img src="../webimages/precipKey.jpg">
				<p><em>Alaska data starts in 1925. Data source: NOAA (National Oceanic and Atmospheric Administration). 2016. National Centers for Environmental Information. Accessed February 2016. <a href="http://www.ncei.noaa.gov" target="_blank">www.ncei.noaa.gov</a>.</em></p>
			</div>

	</div>
	
	<hr/>
	
	<div class="splitCol">

			<div>
				<img src="../webimages/NOAA-CEI-Index.jpg" alt="Contiguous U.S. Climate Extreems Index"/>
			</div>

			<div>
				<h2>CONTIGUOUS U.S. CLIMATE EXTREMES INDEX 1910-2015</h2>
				<p>Climate Extremes Index (CEI) uses extreme temperatures, drought data, extreme precipitation data and number of precipitation days to show extreme weather patterns for given years.  As this graph shows, extreme weather is consistently rising.</p>
				<ul>
					<li class="green">9-Point Binomial Filter</li>
					<li class="red">Mean</li>
					<li>Annual Percent</li>
				</ul>
				
				<p><em>Data source: NOAA (National Oceanic and Atmospheric Administration). 2016. National Centers for Environmental Information. Accessed February 2016. <a href="http://www.ncei.noaa.gov" target="_blank">www.ncei.noaa.gov</a>.</em></p>			</div>

	</div>
<?php include('includes/footer.php'); ?>