<?php $host = $_SERVER['SERVER_NAME']; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="title" content="">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!--[if IE]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<title>Hawkins Flood Protection</title>
	<link rel="shortcut icon" href="favicon.ico">

	<link href="https://fonts.googleapis.com/css?family=Arvo:400,700|Montserrat:300,300i,600" rel="stylesheet">
	<link rel="stylesheet" href="css/layout.min.css" />
</head>

<body<?php if($host == "localhost" || $host == "127.0.0.1") { echo ' id="debug"'; } ?>>
<div id="page">
	<header>
		<section>
		  <a href="index.php" class="logo"> <span class="visuallyhidden">Home</span>
		    <img src="webimages/flood-plan-logo.svg" alt="flood plan logo"/>
		  </a>

			<button class="hamburger hamburger--slider" type="button" id="mobileMenuBtn">
				<span class="label">Menu</span>
			  <span class="hamburger-box">
			    <span class="hamburger-inner"></span>
			  </span>
			</button>

		  <nav>
		    <ul>
		      <li><a href="#">Questions &amp; Answers</a></li>
		      <li><a href="communitymeetings.php">Community Meetings</a></li>
		     <li><a href="timeline.php">Timeline</a></li>
		      <li><a href="resources.php">Resources</a></li>
		    </ul>
		  </nav>
		</section>

		<div class="logos">
			<img src="webimages/logo-metro_gov_seal.jpg" alt="metro gov seal" />
			<img src="webimages/logo-metro_water_services.jpg" alt="metro watever services logo" />
		</div>
	</header>
