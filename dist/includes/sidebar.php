<aside>
  <a href="#" class="logo">
    <img src="webimages/flood-plan-logo.svg" />
  </a>

  <nav>
    <ul>
      <li><a href="#">Questions &amp; Answers</a></li>
      <li><a href="communitymeetings.php">Community Meetings</a></li>
      <li><a href="timeline.php">Timeline</a></li>
      <li><a href="resources.php">Resources</a></li>
    </ul>
  </nav>
</aside>
