
$(document).ready(function() {
  $(".lightgallery").lightGallery();


  $('#mobileMenuBtn').click(function(){
    $(this).toggleClass('is-active');
    $(this).next('nav').toggleClass('expanded');
  });

  var logos = $('header .logos').html();
  $('footer').append(logos);

});
